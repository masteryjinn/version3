import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import { defineRoutes, connection } from './routes.js';
import { Server as SocketIOServer } from 'socket.io';

import { MongoClient } from 'mongodb';
import { ObjectId } from 'mongodb';

// Параметри підключення до MongoDB
const url = 'mongodb://127.0.0.1:27017';
const dbName = 'Messages';

// Підключення до MongoDB
const client = new MongoClient(url);

const app = express();
const port = 3000;
const server = http.createServer(app);

const users = {};

app.use(bodyParser.json());

// CORS middleware
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5501');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

async function connectToMongoDB() {
    try {
        await client.connect();
        console.log('Connected to MongoDB');
        // Перевірка наявності бази даних тут
        const db = client.db(dbName);
        const collections = await db.listCollections({ name: 'Message' }).toArray();
        if (collections.length === 0) {
            console.log('Creating Message collection');
            await db.createCollection('Message');
        }
        const collections2 = await db.listCollections({ name: 'Chat' }).toArray();
        if (collections2.length === 0) {
            console.log('Creating Message collection');
            await db.createCollection('Chat');
        }
    } catch (error) {
        console.error('Failed to connect to MongoDB:', error);
        process.exit(1);
    }
}

// Виклик функції підключення
connectToMongoDB();

async function startServer() {
    try {
        await defineRoutes(app,client);
        console.log('Database and tables created successfully');
        await checkDatabaseExists();
        
        server.listen(port, () => {
            console.log(`Server is running on port ${port}`);
        });

        const io = new SocketIOServer(server, {
            cors: {
                origin: "http://127.0.0.1:5501",
                methods: ["GET", "POST"],
                credentials: true
            }
        });

        // При підключенні нового користувача
        io.on('connection', (socket) => {
            socket.on('login', (email) => {
                users[email] = socket.id;
                console.log(`User ${email} connected`);
            });

            socket.on('message', async (data) => {
                console.log('Message received:', data);
                const { sender, text, chatId } = data;
                saveMessageToDatabase(data);
                if (chatId.includes('all')) {
                    // Якщо чат включає усіх учасників, відправте повідомлення всім підключеним користувачам
                    io.emit('messageResponse', { success: true, message: 'Message sent to all students', data: data });
                } else {
                    // Якщо чат не включає усіх, то потрібно знайти учасників цього чату
                    try {
                        const db = client.db(dbName);

                        // Перетворення рядка chatId на ObjectId
                        const chatObjectId = new ObjectId(chatId);

                        // Пошук чату за допомогою ObjectId
                        const chat = await db.collection('Chat').findOne({ _id: chatObjectId });
                        
                        if (!chat) {
                            io.to(socket.id).emit('messageResponse', { success: false, error: 'Chat not found' });
                            return;
                        }
                        
                        const recipients = chat.participants;
                        
                        // Відправте повідомлення кожному учаснику чату
                        recipients.forEach(recipient => {
                            const recipientSocketId = users[recipient];
                            if (recipientSocketId) {
                                io.to(recipientSocketId).emit('messageResponse', { success: true, data: { sender, text, chatId } });
                            } else {
                                io.to(socket.id).emit('messageResponse', { success: false, error: 'Recipient not found' });
                            }
                        });
                    } catch (error) {
                        console.error('Error fetching chat participants:', error);
                        io.to(socket.id).emit('messageResponse', { success: false, error: 'Failed to fetch chat participants' });
                    }
                }
            });            

            socket.on('disconnect', () => {
                const user = Object.keys(users).find(key => users[key] === socket.id);
                if (user) {
                    delete users[user];
                    console.log(`User ${user} disconnected`);
                }
            });
        });
    } catch (error) {
        console.error('Failed to start server:', error);
        process.exit(1);
    }
}

startServer();

async function checkDatabaseExists() {
    try {
        const [rows, fields] = await connection.query("SHOW DATABASES LIKE 'students_database'");
        if (rows.length === 0) {
            await executeQueries(queries);
            console.log('Database and tables created successfully');
        } else {
            console.log('Database and tables already exist');
        }
    } catch (error) {
        console.error('Failed to check database existence:', error);
        throw error;
    }
}



async function saveMessageToDatabase(message) {
    try {
        const db = client.db(dbName);
        const collection = db.collection('Message');
        
        // Вставляємо повідомлення з ідентифікатором чату у колекцію
        await collection.insertOne(message);
        
        console.log('Message saved to database:', message);
    } catch (error) {
        console.error('Failed to save message to database:', error);
    }
}

