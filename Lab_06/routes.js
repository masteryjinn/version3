// routes.js

import fs from 'fs';
import mysql from 'mysql2/promise';
import { ObjectId } from 'mongodb';

const connection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '12345678',
    database: 'students_database'
});

// Read the content of scheme.sql as a string
const sqlQueries = fs.readFileSync('scheme.sql', 'utf8');

// Split the content into individual queries
const queries = sqlQueries.split(';').filter(query => query.trim() !== ''); // Filter out empty queries

// Function to execute each query in sequence
async function executeQueries(queries) {
    for (const query of queries) {
        try {
            await connection.query(query);
        } catch (error) {
            console.error('Failed to execute query:', error);
            throw error; // Propagate the error
        }
    }
}

async function getAllStudents() {
    const [rows] = await connection.query("select * from Students")
    return rows
}
// Function to define routes
export async function defineRoutes(app,dbClient) {
    try {
        await executeQueries(queries);
        console.log('Database and tables created successfully');
        // Define routes
        app.get("/students", async (req, res) => {
            console.log('get');
            const students = await getAllStudents()
            console.log(students);
            res.send(students)
            })

        app.post('/messages', (req, res) => getMessages(req, res, dbClient));

        app.post('/create-chat', (req, res) => CreateChat(req, res, dbClient));

        app.post('/get-chats', (req, res) => getChats(req, res, dbClient));

        app.post('/get-chat-info', (req, res) => getChatInfo(req, res, dbClient));
            // Перевірка існування студента
        app.post('/checkStudent', async (req, res) => {
            try {
                const { name, surname } = req.body;

                // Виконуємо запит до бази даних, щоб перевірити, чи існує студент з вказаними ім'ям та прізвищем
                const isStudentPresent = await checkStudentPresence(name, surname);

                // Відправляємо відповідь клієнту з результатом перевірки
                res.json({ isStudentPresent });
            } catch (error) {
                console.error('Error:', error);
                res.status(500).json({ error: 'Server error' });
            }
        });

        app.post('/student', async (req, res) => {
            const studentData = req.body;
            console.log(studentData);
            
            // Ensure each required property is present in the request body
            if (!studentData.hasOwnProperty('id') || 
                !studentData.hasOwnProperty('group') || 
                !studentData.hasOwnProperty('name') || 
                !studentData.hasOwnProperty('surname') || 
                !studentData.hasOwnProperty('gender') || 
                !studentData.hasOwnProperty('birthday')) {
                    return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
            }

            // Перевірка коректності даних студента
            if (!isValidStudentData(studentData)) {
                return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
            }

            try {
                const { id, group, name, surname, gender, birthday } = studentData;
                const result = await connection.query('INSERT INTO Students (id, group_name, name, surname, gender, birthday) VALUES (?, ?, ?, ?, ?, ?)', [id, group, name, surname, gender, birthday]);
                res.json({ success: true, message: 'Student added successfully' });
            } catch (error) {
                console.error('Error adding student:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to add student to the database' });
            }
        });

        app.patch('/student', async (req, res) => {
            const studentData = req.body; 
            console.log(studentData);
            
            // Ensure each required property is present in the request body
            if (!studentData.hasOwnProperty('id') || 
                !studentData.hasOwnProperty('group') || 
                !studentData.hasOwnProperty('name') || 
                !studentData.hasOwnProperty('surname') || 
                !studentData.hasOwnProperty('gender') || 
                !studentData.hasOwnProperty('birthday')) {
                    return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
            }

                // Перевірка коректності даних студента
            if (!isValidStudentData(studentData)) {
                return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
            }
        
            try {
                const { id, group, name, surname, gender, birthday } = studentData;
                const result = await connection.query('UPDATE Students SET group_name = ?, name = ?, surname = ?, gender = ?, birthday = ? WHERE id = ?', [group, name, surname, gender, birthday, id]);
                res.json({ success: true, message: 'Student data updated successfully' });
            } catch (error) {
                console.error('Error updating student data:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to update student data' });
            }
        });
        

        app.delete('/student', async (req, res) => {
            const studentId = parseInt(req.body.id, 10); 

            try {
                const result = await connection.query('DELETE FROM Students WHERE id = ?', studentId);
                res.json({ success: true, message: `Student with ID ${studentId} deleted successfully` });
            } catch (error) {
                console.error('Error deleting student:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to delete student from the database' });
            }
        });
    } catch (error) {
        console.error('Failed to initialize routes:', error);
        throw new Error('Failed to initialize routes:', error);
    }
}

// Функція для перевірки коректності даних студента
//поле айді пусте, бо назначається у функції додавання
function isValidStudentData(studentData) {
    const name = studentData.name.trim();   
    const surname = studentData.surname.trim();
    const doubleRegex = /^[A-Z][a-z]*(-[A-Z][a-z]*)?$/;
    
    if (!doubleRegex.test(name) || !doubleRegex.test(surname)) {
        //alert("Please ensure the first letter of the name is capitalized and the surname follows the correct format.");
        return false; 
    }

    return true;
}  

async function getMessages(req, res, dbClient) {
    try {
        const { chatId } = req.body; // Отримуємо айді чату з тіла запиту
        const db = dbClient.db('Messages');

        // Отримуємо повідомлення, де chatId дорівнює chatId з тіла запиту
        const messages = await db.collection('Message').find({ chatId: chatId }).toArray();
        console.log(messages);
        // Відправляємо список повідомлень у відповідь
        res.json(messages);
    } catch (error) {  
        console.error('Error fetching messages:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to fetch messages from the database' });
    }
}

async function getChatsByParticipantId(participantId, dbClient) {
    try {
        const db = dbClient.db('Messages'); // Замініть 'Chat' на назву вашої бази даних

        // Знайдіть всі чати, в яких учасник зазначений учасник
        const chats = await db.collection('Chat').find({ participants: participantId }).toArray();

        return chats;
    } catch (error) {
        console.error('Error fetching chats:', error);
        throw error;
    }
}

// Ручка для отримання чатів за айді учасника
async function getChats(req, res, dbClient) {
    try {
        const participantId = req.body.email; // Отримайте айді учасника (електронна пошта) з тіла запиту
        const chats = await getChatsByParticipantId(participantId, dbClient);

        res.json(chats);
    } catch (error) {
        console.error('Error fetching chats:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to fetch chats from the database' });
    }
}

async function getChatInfo(req, res, dbClient) {
    try {
        const chatId = req.body.chatId; // Отримайте ідентифікатор чату з тіла запиту
        const db = dbClient.db('Messages'); // Отримайте доступ до бази даних через клієнта БД

        // Отримайте інформацію про чат з бази даних за допомогою ідентифікатора
        const chat = await db.collection('Chat').findOne({ _id: new ObjectId(chatId) });

        if (!chat) {
            // Якщо чат не знайдено, відправте відповідь з відповідним повідомленням про помилку
            return res.status(404).json({ error: 'Chat not found', message: 'Chat with the provided ID was not found' });
        }

        // Якщо чат знайдено, відправте його інформацію у відповідь
        res.json(chat);
    } catch (error) {
        console.error('Error fetching chat info:', error);
        // Відправте відповідь про помилку, якщо сталася помилка під час обробки запиту
        res.status(500).json({ error: 'Database error', message: 'Failed to fetch chat info from the database' });
    }
}

async function CreateChat(req, res, dbClient) {
    try {
        const { participants, chatName } = req.body; // Отримайте масив учасників та назву чату з тіла запиту
        // Відкрийте підключення до бази даних
        const db = dbClient.db('Messages');
        // Вставте новий документ у колекцію чатів
        const result = await db.collection('Chat').insertOne({
            participants: participants,
            chatName: chatName
        });
        if (result.acknowledged) {
            // Операція вставки була успішно виконана
            // Отримайте _id нового чату
            const insertedId = result.insertedId;
            res.status(201).json({ message: 'Chat created successfully', chatId: insertedId });
        } else {
            // Помилка при вставці даних
            console.log('Insert operation was not acknowledged');
            res.status(500).json({ error: 'Database error', message: 'Failed to create chat' });
        }        
    } catch (error) {  
        console.error('Error creating chat:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to create chat' });
    }
}

// Функція для перевірки наявності студента за іменем та прізвищем
async function checkStudentPresence(name, surname) {
    try {
        const [rows] = await connection.query('SELECT * FROM Students WHERE name = ? AND surname = ?', [name, surname]);
        return rows.length > 0; // Повертаємо true, якщо студент існує, і false, якщо не існує
    } catch (error) {
        console.error('Error checking student presence:', error);
        throw new Error('Failed to check student presence:', error);
    }
}

export { connection }; // Export the connection variable

