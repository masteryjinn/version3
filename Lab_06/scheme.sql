USE students_database;

DROP TABLE IF EXISTS Students;

CREATE TABLE Students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    group_name VARCHAR(50),
    name VARCHAR(50),
    surname VARCHAR(50),
    gender VARCHAR(10),
    birthday DATE
);


