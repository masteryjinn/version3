var loginBox = document.getElementById("login");
var regBox = document.getElementById("register");
var forgetBox = document.getElementById("forgot");

var cont =document.getElementById("cont");

var loginTab = document.getElementById("lt");
var regTab = document.getElementById("rt");

function regTabFun(){
    event.preventDefault();

    regBox.style.visibility="visible";
    loginBox.style.visibility="hidden";
    forgetBox.style.visibility="hidden";

    cont.style.height = "410px"; 

    regTab.style.backgroundColor="rgb(12, 132, 189)";
    loginTab.style.backgroundColor="rgba(11, 177, 224, 0.82)";
}

function loginTabFun(){
    event.preventDefault();

    regBox.style.visibility="hidden";
    loginBox.style.visibility="visible";
    forgetBox.style.visibility="hidden";

    cont.style.height = "300px";

    loginTab.style.backgroundColor="rgb(12, 132, 189)";
    regTab.style.backgroundColor="rgba(11, 177, 224, 0.82)";
}

function forTabFun(){
    event.preventDefault();

    regBox.style.visibility="hidden";
    loginBox.style.visibility="hidden";
    forgetBox.style.visibility="visible";

    regTab.style.backgroundColor="rgba(11, 177, 224, 0.82)";
    loginTab.style.backgroundColor="rgba(11, 177, 224, 0.82)";
}

var emailArray = JSON.parse(localStorage.getItem('emailArray')) || [];
var passwordArray = JSON.parse(localStorage.getItem('passwordArray')) || [];
var nameArray = JSON.parse(localStorage.getItem('nameArray')) || [];
var surnameArray = JSON.parse(localStorage.getItem('surnameArray')) || [];

async function register() {
    event.preventDefault();
    var email = document.getElementById("re").value;
    var name = document.getElementById("rn").value;
    var surname = document.getElementById("surnameFieldId").value;
    var password = document.getElementById("rp").value;
    var passwordRetype = document.getElementById("rrp").value;

    if (name == "" || surname == "") {
        alert("Name and surname required.");
        return;
    } else if (password == "") {
        alert("Password required.");
        return;
    } else if (passwordRetype == "") {
        alert("Password required.");
        return;
    } else if (password != passwordRetype) {
        alert("Passwords don't match. Please retype your Password.");
        return;
    } else if (emailArray.indexOf(email) != -1) {
        alert(email + " is already registered.");
        return;
    } else {
        // Додати ім'я та прізвище до локального сховища
        nameArray.push(name);
        surnameArray.push(surname);
        localStorage.setItem('nameArray', JSON.stringify(nameArray));
        localStorage.setItem('surnameArray', JSON.stringify(surnameArray));
        
        try {
            // Виконуємо запит на сервер для перевірки існування студента
            const response = await fetch('http://localhost:3000/checkStudent', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ name, surname })
            });
            const data = await response.json();

            if (data.isStudentPresent) {
                // Якщо студент існує, реєструємо його
                emailArray.push(email);
                passwordArray.push(password);
                localStorage.setItem('emailArray', JSON.stringify(emailArray));
                localStorage.setItem('passwordArray', JSON.stringify(passwordArray));

                alert(email + "  Thanks for registration. \nTry to login Now");
                document.getElementById("rn").value ="";
                document.getElementById("surnameFieldId").value="";
                document.getElementById("re").value ="";
                document.getElementById("rp").value="";
                document.getElementById("rrp").value="";
                console.log('Студент знайдений.');
                return;
            } else {
                alert("Student with this name and surname hasn`t registered yet.");
                window.location.href = "index.html";
                return;
            }
        } catch (error) {
            console.error('Error:', error);
            alert('Failed to register. Please try again later.');
        }
    }
}

// Функція для входу користувача
function login() {
    var email = document.getElementById("se").value;
    var password = document.getElementById("sp").value;

    var i = emailArray.indexOf(email);

    if (emailArray.indexOf(email) == -1) {
        if (email == "") {
            alert("Email required.");
            return;
        }
        alert("Email does not exist.");
        return;
    } else if (passwordArray[i] != password) {
        if (password == "") {
            alert("Password required.");
            return;
        }
        alert("Password does not match.");
        return;
    } else {
        alert(email + " you are logged in now. Welcome to our website.");

        // Add redirect to chat.html page with email parameter
        window.location.href = "chat.html?email=" + email;

        document.getElementById("se").value = "";
        document.getElementById("sp").value = "";
        return;
    }
}

function forgot(){
    event.preventDefault();

    var email = document.getElementById("fe").value;

    if(emailArray.indexOf(email) == -1){
        if (email == ""){
            alert("Email required.");
            return ;
        }
        alert("Email does not exist.");
        return ;
    }

    alert("email is send to your email check it in 24hr. \n Thanks");
    document.getElementById("fe").value ="";
}
