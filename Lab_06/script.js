    // Функція для додавання прослуховувача кліків до повідомлень чату
    function addClickListenersToMessages() {
        const chatMessages = document.querySelectorAll('.chat-message');
        chatMessages.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                //const chatInfo = message.dataset.chatInfo;
                //openChat(chatInfo);
            });
        });
    }

document.addEventListener("DOMContentLoaded", async function () {
    let ChatIds = ['all'];
    let currentChatId = null;
   // Оголошуємо та ініціалізуємо emailArray
   var emailArray = JSON.parse(localStorage.getItem('emailArray')) || [];

   // Оголошення змінних для імені та прізвища користувача
   var nameArray = JSON.parse(localStorage.getItem('nameArray')) || [];
   var surnameArray = JSON.parse(localStorage.getItem('surnameArray')) || [];

   // Перевірка, чи є email в URL
   const queryString = window.location.search;
   const urlParams = new URLSearchParams(queryString);
    let email = urlParams.get('email');
    // Якщо email не існує, перенаправляємо на сторінку авторизації
    if (!email) {
        window.location.href = "login.html";
        return;
    }
    loadChatsFromDatabase();
   // Отримання елемента для відображення імені користувача
   var userNameElement = document.getElementById("user-name");

    // Виклик функції зміни імені користувача після отримання всіх необхідних значень
      changeUserName(email, emailArray, nameArray, surnameArray, userNameElement);
  
      // Функція для зміни імені користувача за його електронною адресою
      function changeUserName(email, emailArray, nameArray, surnameArray, userNameElement) {
          // Знаходимо індекс користувача за його електронною адресою
          var index = emailArray.indexOf(email);
          
          // Перевіряємо, чи знайдено користувача
          if (index !== -1) {
              // Отримуємо ім'я та прізвище користувача з локального сховища
              var name = nameArray[index];
              var surname = surnameArray[index];
              
              // Формуємо нове ім'я для користувача
              var fullName = name + " " + surname;
              
              // Оновлюємо відображення імені користувача на сторінці
              userNameElement.textContent = fullName;
          }
      }

    // Знайдіть елемент посилання "Students"
    var studentsLink = document.querySelector('a[href="./index.html"]');

    // Перевірте, чи знайдено елемент посилання
    if (studentsLink) {
        // Змініть атрибут href, додаючи до нього параметр запиту з електронною поштою
        studentsLink.href = "./index.html?email=" + email;
    }
    const socket = io('http://127.0.0.1:3000'); // URL сервера WebSocket

    socket.emit('login', email);

    // Логування помилок на клієнті
    ['connect_error', 'connect_timeout', 'error'].forEach(event => {
        socket.on(event, (error) => {
            console.error(`Socket ${event} error:`, error);
        });
    });

    const notificationButton = document.getElementById("notifications-button");

    notificationButton.ondblclick = () => {
        notificationButton.animate([
            { transform: "rotate(0)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(30deg)" },
            { transform: "rotate(0)" }
        ], {
            duration: 500,
            iterations: 1
        });
    };

    let isClicked= false;
    notificationButton.addEventListener('click', () => {
        const modalProfile = document.getElementById("modal-notifications");
        toggleModal(modalProfile);
        isClicked = !isClicked;
    });

    const userElement = document.getElementById('user');
    userElement.classList.remove("hidden");

    let isProfileOpened = false;
    const profileButton = document.getElementById("user");

    const toggleModal = (modalWindow) => {
        modalWindow.classList.toggle("hidden");
    };

    profileButton.addEventListener('click', () => {
        const modalProfile = document.getElementById("modal-profile");
        toggleModal(modalProfile);
        isProfileOpened = !isProfileOpened;
    });

    document.getElementById('log-out-modal-btn').addEventListener('click', function () {
        window.location.href = "login.html";
    });
    // Функція для створення чату з новим користувачем та додавання його до масиву співрозмовників
    function createChatWithParticipant(sender) {
        conversationParticipants.push(sender);
        const newChatElement = document.createElement('div');
        newChatElement.classList.add('chat-message');
        newChatElement.dataset.chatInfo = sender;
        newChatElement.innerHTML = `
            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
            <span class="chat-username">${sender}</span>
        `;
        document.getElementById('chat-block').appendChild(newChatElement);
        // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
        openChatOnClick();
    }

    function addMessageToChat(message, isSent) {
            const messagesContainer = document.getElementById('chat-room-admin-messages');
            const messageElement = document.createElement('div');
            messageElement.classList.add('chat-message', isSent ? 'sent' : 'received');
            messageElement.innerHTML = `
            <div class="msg-info">
                <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                <span class="username">${message.sender === email ? 'You' : getName(message.sender, emailArray, nameArray, surnameArray, userNameElement)}</span>
            </div>
            <p class="text">${message.text}</p>`;
            messagesContainer.appendChild(messageElement);
            messagesContainer.scrollTop = messagesContainer.scrollHeight;
    }    

    async function getMessagesFromServer(chatId) {
        try {
            const response = await fetch('http://localhost:3000/messages', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ chatId: chatId })
            });
            if (response.ok) {
                const messages = await response.json();
                return messages;
            } else {
                console.error('Failed to fetch messages:', response.statusText);
                return [];
            }
        } catch (error) {
            console.error('Error fetching messages:', error);
            return [];
        }
    }

    function sendMessage(messageText, chatId) {
        try {
            socket.emit('message', { sender: email, text: messageText, chatId: chatId });
            document.getElementById("message-input").value = ''; // Очищення поля вводу після надсилання
        } catch (error) {
            console.error('Error while sending message:', error);
            alert('Failed to send message. Please try again later.');
        }
    }    

    // Функція для відображення спливаючого повідомлення
    function showNotification(sender, text) {
        if(sender===email)
            return;
        const notificationsWindow = document.getElementById("modal-notifications");

        // Перевірка, чи вікно приховане, і якщо так, то змінюємо його стиль, щоб воно стало видимим
        if (notificationsWindow.classList.contains("hidden")) {
            notificationsWindow.classList.remove("hidden");
        }
        
        // Створення нового елемента для відображення останнього повідомлення
        const messageElement = document.createElement('div');
        messageElement.classList.add('notification-item');
        messageElement.innerHTML = `
            <div class="notification-sender">${sender}</div>
            <div class="notification-text">${text}</div>
            <i class="fa fa-times notification-close"></i>
        `;
        
        // Додавання нового повідомлення до початку спливаючого вікна
        notificationsWindow.insertBefore(messageElement, notificationsWindow.firstChild);
    }

    // Додавання обробника подій до контейнера сповіщень
    document.getElementById("modal-notifications").addEventListener('click', (event) => {
        // Перевірка, чи клік відбувся на кнопці хрестика
        if (event.target.classList.contains('notification-close')) {
            const notificationItem = event.target.parentNode; // Отримуємо батьківський елемент (повідомлення)
            notificationItem.remove(); // Видалення повідомлення
        }
    });

    // Обробник отриманих повідомлень
    socket.on('messageResponse', async (response) => {
        if (response.success) {
            const newMessage = { sender: response.data.sender, text: response.data.text, chatId: response.data.chatId };
            if (!ChatIds.includes(newMessage.chatId)) {
                try {
                    // Відправити запит на сервер для створення чату з відсутнім ідентифікатором
                    const serverResponse = await fetch('http://localhost:3000/get-chat-info', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({ chatId: newMessage.chatId }) // Передайте ідентифікатор чату
                    });
                    
                    if (!serverResponse.ok) {
                        throw new Error('Failed to create chat on server');
                    }
                    
                    // Обробка отриманих даних з сервера, якщо потрібно
                    const responseData = await serverResponse.json();
                    const newChatElement = document.createElement('div');
                    newChatElement.classList.add('chat-message');
                    newChatElement.dataset.chatInfo = Object.values(responseData.participants).join(',');
                    newChatElement.dataset.chatId = responseData._id;
                    ChatIds.push(responseData._id);
                    newChatElement.innerHTML = `
                        <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                        <span class="chat-username">${responseData.chatName}</span>
                    `;
                    document.getElementById('chat-block').appendChild(newChatElement);
                    // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
                    openChatOnClick();
                    console.log('Chat:', responseData);
                    
                } catch (error) {
                    console.error('Error while creating chat:', error);
                    // Обробка помилки створення чату
                }
            }
            if(newMessage.chatId===currentChatId){
                addMessageToChat(newMessage, response.data.sender === email); // Додати повідомлення у чат як отримане
                // Очищення поля вводу після отримання відповіді
                document.getElementById("message-input").value = '';
                // Додавання прослуховувачів кліків до повідомлень
                addClickListenersToMessages();
            }
            // Показ повідомлення в спливаючому вікні
            showNotification(newMessage.sender, newMessage.text);
        } else {
            console.error('Message failed to send:', response.error);
            alert(response.error);
        }
    });         

    const sendButton = document.getElementById("send-button");
    sendButton.addEventListener('click', () => {
        const messageInput = document.getElementById("message-input");
        const messageText = messageInput.value.trim();
        if (messageText !== '') {
            const chatId = document.querySelector('.chat-message.selected').dataset.chatId;
            sendMessage(messageText, chatId);
        }
        addClickListenersToMessages();
    });

      // Функція для зміни імені користувача за його електронною адресою
      function getName(email, emailArray, nameArray, surnameArray, userNameElement) {
          // Знаходимо індекс користувача за його електронною адресою
          var index = emailArray.indexOf(email);
          
          // Перевіряємо, чи знайдено користувача
          if (index !== -1) {
              // Отримуємо ім'я та прізвище користувача з локального сховища
              var name = nameArray[index];
              var surname = surnameArray[index];
              
              // Формуємо нове ім'я для користувача
              var fullName = name + " " + surname;
              return fullName; 
          } else {
              return "Unknown User";
          }
      }

    async function openChat(chatInfo) {
        const participants = chatInfo.split(',').map(recipient => recipient.trim());
        updateMembers(participants);
        document.getElementById('chat-room-admin-header').innerHTML = `<h2>Chat Room with ${participants.join(', ')}</h2>`;
        document.getElementById('chat-room-admin').style.display = 'flex';
        
        // Очищення контейнера повідомлень перед відображенням нових повідомлень
        const messagesContainer = document.getElementById('chat-room-admin-messages');
        messagesContainer.innerHTML = '';
            try {
                const messages = await getMessagesFromServer(currentChatId);
                const messageArray = Object.values(messages);
                messageArray.forEach(message => {
                    const messageElement = document.createElement('div');
                    messageElement.classList.add('chat-message', message.sender === email ? 'sent' : 'received');
                    messageElement.innerHTML = `
                        <div class="msg-info">
                            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                            <span class="username">${message.sender === email ? 'You' : getName(message.sender, emailArray, nameArray, surnameArray, userNameElement)}</span>
                        </div>
                        <p class="text">${message.text}</p>`;
                    messagesContainer.appendChild(messageElement);
                });
            } catch (error) {
                console.error('Error fetching messages:', error);
            }
        addClickListenersToMessages();
    }      

    function updateMembers(participants) {
        const membersContainer = document.getElementById('chat-room-admin-members');
        membersContainer.innerHTML = '';
        participants.forEach(participant => {
            const userIcon = document.createElement('i');
            userIcon.classList.add('fa', 'fa-user-circle');
            membersContainer.appendChild(userIcon);
        });
        const plusIcon = document.createElement('i');
        plusIcon.classList.add('fa', 'fa-plus-circle');
        membersContainer.appendChild(plusIcon);
    }

    function openChatOnClick() {
        const newChatElement = document.querySelectorAll('.chat-message');
        newChatElement.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                const chatInfo = message.dataset.chatInfo;
                if(currentChatId===message.dataset.chatId)
                    return;
                currentChatId=message.dataset.chatId;
                openChat(chatInfo);
            });
        });
    }

    const chatMessages = document.querySelectorAll('.chat-message');
    chatMessages.forEach(message => {
        message.addEventListener('click', () => {
            document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
            message.classList.add('selected');
            const chatInfo = message.dataset.chatInfo;
            if(currentChatId===message.dataset.chatId)
            return;
            currentChatId=message.dataset.chatId;
            openChat(chatInfo);
        });
    });

    const addChatButton = document.getElementById("new-chat-room-link");
    const addChatModalWrapper = document.getElementById("add-chat");
    const addChatForm = document.getElementById("add-chat-form");
    const addChatModal = addChatModalWrapper?.querySelector(".modal");

    addChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
    addChatForm.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const chatName = formData.get('chat-name');
        const participants = formData.get('participants').split(',');
        // Перевірте, чи email вже є учасником чату
        if (!participants.includes(email)) {
            participants.push(email); // Додайте email до масиву учасників
        }
    
        try {
            const response = await fetch('http://localhost:3000/create-chat', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ chatName: chatName, participants: participants })
            });
    
            if (!response.ok) {
                throw new Error('Failed to create chat');
            }
    
            const responseData = await response.json();
            // Отримайте _id нового чату
            const chatId = responseData.chatId;
    
            // Додати новий чат до відображення на сторінці
            const newChatElement = document.createElement('div');
            newChatElement.classList.add('chat-message');
            // Додати _id до dataset
            newChatElement.dataset.chatInfo = `${participants.join(',')}`;
            newChatElement.dataset.chatId = chatId; // Додайте _id до dataset
            ChatIds.push(chatId); // Додайте _id до масиву ChatIds
            newChatElement.innerHTML = `
                <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                <span class="chat-username">${chatName}</span>
            `;
            document.getElementById('chat-block').appendChild(newChatElement);
    
            toggleModal(addChatModalWrapper);
    
            // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
            openChatOnClick();
        } catch (error) {
            console.error('Error creating chat:', error);
            // Обробка помилки створення чату
        }
    });     

    const cancelAddChatButton = document.getElementById("add-chat-btn-close");
    cancelAddChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));

    async function loadChatsFromDatabase() {
        try {
            // Відправити запит на сервер для отримання чатів, до яких належить користувач з даною електронною поштою
            const response = await fetch('http://localhost:3000/get-chats', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ email: email })
            });
    
            if (!response.ok) {
                throw new Error('Failed to fetch chats');
            }
    
            const chats = await response.json();
            // Обробити отримані дані про чати, наприклад, вивести їх на сторінці
            chats.forEach(chat =>{
                const newChatElement = document.createElement('div');
                newChatElement.classList.add('chat-message');
                newChatElement.dataset.chatInfo = Object.values(chat.participants).join(',');
                newChatElement.dataset.chatId = chat._id;
                ChatIds.push(chat._id);
                newChatElement.innerHTML = `
                    <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                    <span class="chat-username">${chat.chatName}</span>
                `;
                document.getElementById('chat-block').appendChild(newChatElement);
                // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
                openChatOnClick();
            });
            document.querySelector('.site').classList.remove('hidden');
            console.log('Chats:', chats);
        } catch (error) {
            console.error('Error fetching chats:', error);
            // Обробити помилку отримання чатів
        }
    }

    addClickListenersToMessages();
});
