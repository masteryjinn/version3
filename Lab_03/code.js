document.addEventListener("DOMContentLoaded", function() {
    const studentsTable = document.getElementById("students-table");
    const notificationButton = document.getElementById("notifications-button");
    const profileButton = document.getElementById("user");
    const addStudentButton = document.getElementById("add-student-btn");
    const addStudentModal = document.getElementById("add-student");
    const addStudentForm = document.getElementById("add-student-form");
    const cancelAddStudentButton = document.getElementById("add-student-btn-close");
    const labelAddEdit = document.getElementById("add-student-label");
    const labelDelete = document.getElementById("delete-student-label");

    let isProfileOpened = false;
    let lastStudentId = 0;
    let currentStudentRow = null; // Global variable to store the current student row being edited

    notificationButton.ondblclick = () => {
        notificationButton.animate([
            { transform: "rotate(0)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(30deg)" },
            { transform: "rotate(0)" }
        ], {
            duration: 500,
            iterations: 1
        });
    };

    notificationButton.onmouseover = () => show(document.getElementById("modal-notifications"));
    notificationButton.onmouseleave = () => hide(document.getElementById("modal-notifications"));

    profileButton.onclick = () => {
        if (!isProfileOpened) {
            show(document.getElementById("modal-profile"));
        } else {
            hide(document.getElementById("modal-profile"));
        }
        isProfileOpened = !isProfileOpened;
    };

    addStudentButton.onclick = () => {
        if (currentStudentRow) {
            addStudentForm.reset();
            currentStudentRow = null; // Reset current student row
        }
        show(addStudentModal);
    };

    async function sendDataToServer(data, method = 'POST') {
        try {
            const url = 'http://localhost:3000/student';
            const response = await fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
    
            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(errorData.message);
            }
    
            const responseData = await response.json();
            console.log("Student data sent successfully");
            return responseData;
        } catch (error) {
            console.error("Error sending data:", error);
            throw error;
        }
    }
    
    
    
    addStudentForm.onsubmit = async (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
    
        try {
            let response; // Оголошуємо змінну для результату запиту
            if (currentStudentRow) {
                // Відправляємо дані на сервер методом PUT
                response = await sendDataToServer(Object.fromEntries(formData), 'PATCH');
            } else {
                // Відправляємо дані на сервер методом POST
                response = await sendDataToServer(Object.fromEntries(formData));
            }
    
            if (response && response.success) {
                // Додаємо нового студента у таблицю
                addOrUpdateStudent(Object.fromEntries(formData));
                hide(addStudentModal);
                labelAddEdit.textContent = "Add student";
                console.log("Student updated successfully");
            }
        } catch (error) {
            alert(error); // Виводимо текст помилки
        }
    };
    
    

    cancelAddStudentButton.onclick = () => {
        hide(addStudentModal);
        labelAddEdit.textContent="Add student";
    };

    function removeStudent(studentId) {
        const studentRow = document.getElementById(studentId);
        if (studentRow) {
            const group = studentRow.cells[1].textContent;
            const nameSurname = studentRow.cells[2].textContent;
            const [name, surname] = nameSurname.split(" ");
            const gender = studentRow.cells[3].textContent;
            const birthday = studentRow.cells[4].textContent;
    
            // Create an object with the deleted student's information
            const deletedStudent = {
                id: studentId,
                group: group,
                name: name,
                surname: surname,
                gender: gender,
                birthday: birthday
            };
    
            // Remove the student row from the table
            studentRow.remove();
        }
    }    

    function addOrUpdateStudent(formData) {
        const studentId = formData.id;
        const group = formData.group;
        const name = formData.name;
        const surname = formData.surname;
        const gender = formData.gender;
        const birthday = formData.birthday;

        // Check if we are editing an existing student
        if (currentStudentRow) {
            currentStudentRow.cells[1].textContent = group;
            currentStudentRow.cells[2].textContent = `${name} ${surname}`;
            currentStudentRow.cells[3].textContent = gender;
            currentStudentRow.cells[4].textContent = birthday;
            // Reset currentStudentRow after editing
            currentStudentRow = null;
        } else {
            // Add new student to the table
            lastStudentId++;
            const newStudentId = `student-${lastStudentId}`;
            const studentData = { id: newStudentId, group, name, surname, gender, birthday };
            const studentDataString = addStudent(studentData);
            console.log(studentDataString);
        }
        addStudentForm.reset();
    }        

    /*function validateForm() {
        const name = document.getElementById("name").value.trim();
        const surname = document.getElementById("surname").value.trim();
        const doubleRegex = /^[A-Z][a-z]*(-[A-Z][a-z]*)?$/;
    
        if (!doubleRegex.test(name) || !doubleRegex.test(surname)) {
            alert("Please ensure the first letter of the name is capitalized and the surname follows the correct format.");
            return false; 
        }

        return true;
    }*/
    

    // Function to add a student to the table
    function addStudent({ id, group, name, surname, gender, birthday }) {
        let tr = document.getElementById(id);

        if (!tr) {
            // Create elements for the new student row if it doesn't exist
            tr = document.createElement("tr");
            tr.setAttribute('id', id); // Set the ID for the student row

            const checkboxTd = document.createElement("td");
            const groupTd = document.createElement("td");
            const nameSurnameTd = document.createElement("td");
            const genderTd = document.createElement("td");
            const birthdayTd = document.createElement("td");
            const statusTd = document.createElement("td");
            const actionsContainer = document.createElement("td");

            const checkbox = document.createElement("input");
            const editButton = document.createElement("button");
            const deleteButton = document.createElement("button");

            editButton.classList.add("student__btn");
            editButton.id = "edit-student-btn";
            deleteButton.classList.add("student__btn");
            deleteButton.id = "delete-student-btn";

            checkbox.type = "checkbox";
            checkboxTd.appendChild(checkbox);
            groupTd.textContent = group;
            nameSurnameTd.textContent = `${name} ${surname}`;
            genderTd.textContent = gender;
            birthdayTd.textContent = birthday;
            statusTd.innerHTML = '<i class="fa fa-circle" style="color: #d8d8d8;"></i>';
            editButton.innerHTML = '<i class="fa fa-pencil btn__icon" aria-hidden="true"></i>';
            deleteButton.innerHTML = '<i class="fa fa-trash btn__icon" aria-hidden="true"></i>';

            deleteButton.addEventListener("click", () => {
                const studentRow = document.getElementById(id);
                if (studentRow) {
                    const nameSurname = studentRow.cells[2].textContent;
                    const [name, surname] = nameSurname.split(" ");
                    labelDelete.textContent = "Delete user " + name + " " + surname + " ?"; 
                    show(document.getElementById("delete-warn-student"));
                    const cancelModalButton = document.getElementById("cancel-modal-btn");
                    const deleteModalButton = document.getElementById("delete-modal-btn");
            
                    cancelModalButton.onclick = () => hide(document.getElementById("delete-warn-student"));
            
                    deleteModalButton.onclick = () => {
                        removeStudent(id);
                        hide(document.getElementById("delete-warn-student"));
                    };
                }
            });
            

            editButton.addEventListener("click", () => {
                labelAddEdit.textContent="Edit student";
                populateEditForm(id);
                show(addStudentModal);
            });

            actionsContainer.classList.add("table_buttons");
            actionsContainer.appendChild(editButton);
            actionsContainer.appendChild(deleteButton);

            tr.appendChild(checkboxTd);
            tr.appendChild(groupTd);
            tr.appendChild(nameSurnameTd);
            tr.appendChild(genderTd);
            tr.appendChild(birthdayTd);
            tr.appendChild(statusTd);
            tr.appendChild(actionsContainer);

            studentsTable.appendChild(tr); // Append the student row to the table
        }

        // Return the created or existing student row
        return tr;
    }

    function populateEditForm(studentId) {
        const studentRow = document.getElementById(studentId);
        if (studentRow) {
            const group = studentRow.cells[1].textContent;
            const nameSurname = studentRow.cells[2].textContent;
            const [name, surname] = nameSurname.split(" ");
            const gender = studentRow.cells[3].textContent;
            const birthday = studentRow.cells[4].textContent;

            document.getElementById("group").value = group;
            document.getElementById("name").value = name;
            document.getElementById("surname").value = surname;
            document.getElementById("gender").value = gender;
            document.getElementById("birthday").value = birthday;

            currentStudentRow = studentRow; // Set the current student row
        }
    }

    function show(modalWindow) {
        modalWindow.classList.remove("hidden");
    }

    function hide(modalWindow) {
        modalWindow.classList.add("hidden");
    }
});

