const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

// Встановлюємо middleware для обробки JSON даних
app.use(bodyParser.json());

// Middleware для дозволу CORS
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5500'); // Замініть цей URL на ваш домен
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

// Роут для обробки даних від клієнта
app.post('/student', (req, res) => {
    const studentData = req.body;

    // Перевірка коректності даних студента
    if (!isValidStudentData(studentData)) {
        return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
    }

    // Повертаємо дані у форматі JSON
    res.json({ success: true, message: 'Student data received successfully' });
});

// Роут для оновлення даних студента
app.patch('/student', (req, res) => {
    const studentId = req.params.id;
    const studentData = req.body; // Отримуємо дані студента з запиту

    // Перевірка коректності даних студента
    if (!isValidStudentData(studentData)) {
        return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
    }

    // Логіка оновлення даних студента за ідентифікатором `studentId`
    res.json({ success: true, message: `Student data updated successfully` });
});

// Функція для перевірки коректності даних студента
//поле айді пусте, бо назначається у функції додавання
function isValidStudentData(studentData) {
    const name = studentData.name.trim();   
    const surname = studentData.surname.trim();
    const doubleRegex = /^[A-Z][a-z]*(-[A-Z][a-z]*)?$/;
    
    if (!doubleRegex.test(name) || !doubleRegex.test(surname)) {
        //alert("Please ensure the first letter of the name is capitalized and the surname follows the correct format.");
        return false; 
    }

    return true;
}

// Запускаємо сервер
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

/*
// Роут для обробки DELETE запитів
app.delete('/data/:id', (req, res) => {
    const id = req.params.id;
    // Видалення даних за вказаним ідентифікатором
    res.json({ message: `Data with ID ${id} deleted successfully` });
});
*/