const CACHE_NAME = 'my-cache';

    const urlsToCache = [
        '/',
        '/index.html',
        '/style1.css',
        '/code.js'
    ];

    self.addEventListener('install', event => {
        event.waitUntil(
            caches.open(CACHE_NAME)
                .then(cache => cache.addAll(urlsToCache))
        );
    });

    self.addEventListener('fetch', event => {
        event.respondWith(
            caches.match(event.request)
                .then(cachedResponse => {
                    // Повертаємо кешовану відповідь, якщо вона є
                    if (cachedResponse) {
                        return cachedResponse;
                    }
    
                    // Якщо ресурсу немає в кеші, виконуємо запит до мережі
                    return fetch(event.request)
                        .then(networkResponse => {
                            // Клонуємо відповідь, щоб використати її одночасно у кеші і для відображення
                            const responseToCache = networkResponse.clone();
    
                            // Зберігаємо відповідь в кеші
                            caches.open(CACHE_NAME)
                                .then(cache => {
                                    cache.put(event.request, responseToCache);
                                });
    
                            // Повертаємо відповідь мережі
                            return networkResponse;
                        });
                })
        );
    });
    
