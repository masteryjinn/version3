document.addEventListener("DOMContentLoaded", function () {
    // Перевірка, чи є email в URL
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const email = urlParams.get('email');
    let id_num=6;
    // Якщо email не існує, перенаправляємо на сторінку авторизації
    if (!email) {
        window.location.href = "login.html";
        return;
    }

    const socket = io('http://127.0.0.1:3000'); // URL сервера WebSocket

    socket.emit('login', email);

    // Логування помилок на клієнті
    ['connect_error', 'connect_timeout', 'error'].forEach(event => {
        socket.on(event, (error) => {
            console.error(`Socket ${event} error:`, error);
        });
    });

    const userNameElement = document.getElementById('user-name');
    userNameElement.textContent = email;

    const userElement = document.getElementById('user');
    userElement.classList.remove("hidden");

    let isProfileOpened = false;
    const profileButton = document.getElementById("user");

    const toggleModal = (modalWindow) => {
        modalWindow.classList.toggle("hidden");
    };

    profileButton.addEventListener('click', () => {
        const modalProfile = document.getElementById("modal-profile");
        toggleModal(modalProfile);
        isProfileOpened = !isProfileOpened;
    });

    document.getElementById('log-out-modal-btn').addEventListener('click', function () {
        window.location.href = "login.html";
    });

    function addMessageToChat(message, isSent, chatId) {
        const messagesContainer = document.getElementById('chat-room-admin-messages');
    
        const currentChat = document.querySelector(`.chat-message[data-chat-info="${chatId}"].selected`);
        if (currentChat) {
            const messageElement = document.createElement('div');
            messageElement.classList.add('chat-message', isSent ? 'sent' : 'received');
            messageElement.dataset.chatInfo = chatId; // Встановлюємо нікнейм як ідентифікатор чату
            messageElement.innerHTML = `
                <div class="msg-info">
                    <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                    <span class="username">${isSent ? 'You' : message.sender}</span>
                </div>
                <p class="text">${message.text}</p>`;
            messagesContainer.appendChild(messageElement);
            messagesContainer.scrollTop = messagesContainer.scrollHeight;
        } else {
            addMessageToLocalStorage(chatId, message);
        }
    }
           
    
    // Функція для зберігання повідомлень у локальному сховищі за айді чату
    function addMessageToLocalStorage(chatId, message) {
        let messages = localStorage.getItem(chatId);
        if (!messages) {
            messages = [];
        } else {
            messages = JSON.parse(messages);
        }
        messages.push(message);
        localStorage.setItem(chatId, JSON.stringify(messages));
    }

    // Функція для отримання повідомлень з локального сховища за айді чату
    function getMessagesFromLocalStorage(chatId) {
        const messages = localStorage.getItem(chatId);
        return messages ? JSON.parse(messages) : [];
    }

    function sendMessage(messageText, recipients, chatId) {
        try {
            socket.emit('message', { sender: email, text: messageText, recipients: recipients });
            const newMessage = { sender: email, text: messageText };
            if (!recipients.includes('all') && !recipients.includes(email)) { // Перевірка, чи ви не є одним з отримувачів
                addMessageToChat(newMessage, true);
            }
            document.getElementById("message-input").value = ''; // Очищення поля вводу після надсилання
        } catch (error) {
            console.error('Error while sending message:', error);
            alert('Failed to send message. Please try again later.');
        }
    }    
    
    socket.on('messageResponse', (response) => {
        if (response.success) {
            const newMessage = { sender: response.data.sender, text: response.data.text };
            // Перевіряємо, чи є надсилальником поточний користувач
            const isSentByCurrentUser = response.data.sender === email;
            addMessageToChat(newMessage, isSentByCurrentUser);
            document.getElementById("message-input").value = '';
            // Додати прослуховувачів кліків до повідомлень
            addClickListenersToMessages();
        } else {
            console.log('Message failed to send:', response.error);
            alert(response.error);
        }
    });    

    // Функція для додавання прослуховувача кліків до повідомлень чату
    function addClickListenersToMessages() {
        const chatMessages = document.querySelectorAll('.chat-message');
        chatMessages.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                const chatInfo = message.dataset.chatInfo;
                openChat(chatInfo);
            });
        });
    }

    const sendButton = document.getElementById("send-button");
    sendButton.addEventListener('click', () => {
        const messageInput = document.getElementById("message-input");
        const messageText = messageInput.value.trim();
        if (messageText !== '') {
            const chatInfo = document.querySelector('.chat-message.selected').dataset.chatInfo;
            const recipients = chatInfo.split(',').map(recipient => recipient.trim());
            sendMessage(messageText, recipients, chatInfo); // Передайте айді чату у функцію sendMessage
        }
        addClickListenersToMessages();
    });

    function openChat(chatInfo) {
        const participants = chatInfo.split(',').map(participant => participant.trim());
        updateMembers(participants);

        const messages = getMessagesFromLocalStorage(chatInfo);

        document.getElementById('chat-room-admin-header').innerHTML = `<h2>Chat Room with ${participants.join(', ')}</h2>`;
        document.getElementById('chat-room-admin').style.display = 'flex';
        const messagesContainer = document.getElementById('chat-room-admin-messages');
        messagesContainer.innerHTML = '';
        messages.forEach(message => {
            const messageElement = document.createElement('div');
            messageElement.classList.add('chat-message', 'received');
            messageElement.innerHTML = `
                <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                <span class="chat-username">${message.sender}</span>
                <p class="message-text">${message.text}</p>
            `;
            messagesContainer.appendChild(messageElement);
        });
        addClickListenersToMessages();
    }

    function updateMembers(participants) {
        const membersContainer = document.getElementById('chat-room-admin-members');
        membersContainer.innerHTML = '';
        participants.forEach(participant => {
            const userIcon = document.createElement('i');
            userIcon.classList.add('fa', 'fa-user-circle');
            membersContainer.appendChild(userIcon);
        });
        const plusIcon = document.createElement('i');
        plusIcon.classList.add('fa', 'fa-plus-circle');
        membersContainer.appendChild(plusIcon);
    }

    function openChatOnClick() {
        const newChatElement = document.querySelectorAll('.chat-message');
        newChatElement.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                const chatInfo = message.dataset.chatInfo;
                openChat(chatInfo);
            });
        });
    }

    const chatMessages = document.querySelectorAll('.chat-message');
    chatMessages.forEach(message => {
        message.addEventListener('click', () => {
            document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
            message.classList.add('selected');
            const chatInfo = message.dataset.chatInfo;
            openChat(chatInfo);
        });
    });

    const addChatButton = document.getElementById("new-chat-room-link");
    const addChatModalWrapper = document.getElementById("add-chat");
    const addChatForm = document.getElementById("add-chat-form");
    const addChatModal = addChatModalWrapper?.querySelector(".modal");

    addChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
    addChatForm.addEventListener('submit', event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const chatName = formData.get('chat-name');
        const participants = formData.get('participants').split(',');
        const newChatElement = document.createElement('div');
        newChatElement.classList.add('chat-message');
        newChatElement.dataset.chatInfo = formData.get('participants');
        newChatElement.innerHTML = `
            <input type="hidden" name="id" id="chat-id-">${participants}<" />
            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
            <span class="chat-username">${chatName}</span>
        `;
        document.addEventListener("DOMContentLoaded", function () {
            // Перевірка, чи є email в URL
            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            const email = urlParams.get('email');
            let id_num=6;
            // Якщо email не існує, перенаправляємо на сторінку авторизації
            if (!email) {
                window.location.href = "login.html";
                return;
            }
        
            const socket = io('http://127.0.0.1:3000'); // URL сервера WebSocket
        
            socket.emit('login', email);
        
            // Логування помилок на клієнті
            ['connect_error', 'connect_timeout', 'error'].forEach(event => {
                socket.on(event, (error) => {
                    console.error(`Socket ${event} error:`, error);
                });
            });
        
            const userNameElement = document.getElementById('user-name');
            userNameElement.textContent = email;
        
            const userElement = document.getElementById('user');
            userElement.classList.remove("hidden");
        
            let isProfileOpened = false;
            const profileButton = document.getElementById("user");
        
            const toggleModal = (modalWindow) => {
                modalWindow.classList.toggle("hidden");
            };
        
            profileButton.addEventListener('click', () => {
                const modalProfile = document.getElementById("modal-profile");
                toggleModal(modalProfile);
                isProfileOpened = !isProfileOpened;
            });
        
            document.getElementById('log-out-modal-btn').addEventListener('click', function () {
                window.location.href = "login.html";
            });
        
            function addMessageToChat(message, isSent, chatId) {
                const messagesContainer = document.getElementById('chat-room-admin-messages');
            
                const currentChat = document.querySelector(`.chat-message[data-chat-info="${chatId}"].selected`);
                if (currentChat) {
                    const messageElement = document.createElement('div');
                    messageElement.classList.add('chat-message', isSent ? 'sent' : 'received');
                    messageElement.dataset.chatInfo = chatId; // Встановлюємо нікнейм як ідентифікатор чату
                    messageElement.innerHTML = `
                        <div class="msg-info">
                            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                            <span class="username">${isSent ? 'You' : message.sender}</span>
                        </div>
                        <p class="text">${message.text}</p>`;
                    messagesContainer.appendChild(messageElement);
                    messagesContainer.scrollTop = messagesContainer.scrollHeight;
                } else {
                    addMessageToLocalStorage(chatId, message);
                }
            }
                   
            
            // Функція для зберігання повідомлень у локальному сховищі за айді чату
            function addMessageToLocalStorage(chatId, message) {
                let messages = localStorage.getItem(chatId);
                if (!messages) {
                    messages = [];
                } else {
                    messages = JSON.parse(messages);
                }
                messages.push(message);
                localStorage.setItem(chatId, JSON.stringify(messages));
            }
        
            // Функція для отримання повідомлень з локального сховища за айді чату
            function getMessagesFromLocalStorage(chatId) {
                const messages = localStorage.getItem(chatId);
                return messages ? JSON.parse(messages) : [];
            }
        
            function sendMessage(messageText, recipients, chatId) {
                try {
                    socket.emit('message', { sender: email, text: messageText, recipients: recipients });
                    const newMessage = { sender: email, text: messageText };
                    if (!recipients.includes('all') && !recipients.includes(email)) { // Перевірка, чи ви не є одним з отримувачів
                        addMessageToChat(newMessage, true);
                    }
                    document.getElementById("message-input").value = ''; // Очищення поля вводу після надсилання
                } catch (error) {
                    console.error('Error while sending message:', error);
                    alert('Failed to send message. Please try again later.');
                }
            }    
            
            socket.on('messageResponse', (response) => {
                if (response.success) {
                    const newMessage = { sender: response.data.sender, text: response.data.text };
                    // Перевіряємо, чи є надсилальником поточний користувач
                    const isSentByCurrentUser = response.data.sender === email;
                    addMessageToChat(newMessage, isSentByCurrentUser);
                    document.getElementById("message-input").value = '';
                    // Додати прослуховувачів кліків до повідомлень
                    addClickListenersToMessages();
                } else {
                    console.log('Message failed to send:', response.error);
                    alert(response.error);
                }
            });    
        
            // Функція для додавання прослуховувача кліків до повідомлень чату
            function addClickListenersToMessages() {
                const chatMessages = document.querySelectorAll('.chat-message');
                chatMessages.forEach(message => {
                    message.addEventListener('click', () => {
                        document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                        message.classList.add('selected');
                        const chatInfo = message.dataset.chatInfo;
                        openChat(chatInfo);
                    });
                });
            }
        
            const sendButton = document.getElementById("send-button");
            sendButton.addEventListener('click', () => {
                const messageInput = document.getElementById("message-input");
                const messageText = messageInput.value.trim();
                if (messageText !== '') {
                    const chatInfo = document.querySelector('.chat-message.selected').dataset.chatInfo;
                    const recipients = chatInfo.split(',').map(recipient => recipient.trim());
                    sendMessage(messageText, recipients, chatInfo); // Передайте айді чату у функцію sendMessage
                }
                addClickListenersToMessages();
            });
        
            function openChat(chatInfo) {
                const participants = chatInfo.split(',').map(participant => participant.trim());
                updateMembers(participants);
        
                const messages = getMessagesFromLocalStorage(chatInfo);
        
                document.getElementById('chat-room-admin-header').innerHTML = `<h2>Chat Room with ${participants.join(', ')}</h2>`;
                document.getElementById('chat-room-admin').style.display = 'flex';
                const messagesContainer = document.getElementById('chat-room-admin-messages');
                messagesContainer.innerHTML = '';
                messages.forEach(message => {
                    const messageElement = document.createElement('div');
                    messageElement.classList.add('chat-message', 'received');
                    messageElement.innerHTML = `
                        <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                        <span class="chat-username">${message.sender}</span>
                        <p class="message-text">${message.text}</p>
                    `;
                    messagesContainer.appendChild(messageElement);
                });
                addClickListenersToMessages();
            }
        
            function updateMembers(participants) {
                const membersContainer = document.getElementById('chat-room-admin-members');
                membersContainer.innerHTML = '';
                participants.forEach(participant => {
                    const userIcon = document.createElement('i');
                    userIcon.classList.add('fa', 'fa-user-circle');
                    membersContainer.appendChild(userIcon);
                });
                const plusIcon = document.createElement('i');
                plusIcon.classList.add('fa', 'fa-plus-circle');
                membersContainer.appendChild(plusIcon);
            }
        
            function openChatOnClick() {
                const newChatElement = document.querySelectorAll('.chat-message');
                newChatElement.forEach(message => {
                    message.addEventListener('click', () => {
                        document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                        message.classList.add('selected');
                        const chatInfo = message.dataset.chatInfo;
                        openChat(chatInfo);
                    });
                });
            }
        
            const chatMessages = document.querySelectorAll('.chat-message');
            chatMessages.forEach(message => {
                message.addEventListener('click', () => {
                    document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                    message.classList.add('selected');
                    const chatInfo = message.dataset.chatInfo;
                    openChat(chatInfo);
                });
            });
        
            const addChatButton = document.getElementById("new-chat-room-link");
            const addChatModalWrapper = document.getElementById("add-chat");
            const addChatForm = document.getElementById("add-chat-form");
            const addChatModal = addChatModalWrapper?.querySelector(".modal");
        
            addChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
            addChatForm.addEventListener('submit', event => {
                event.preventDefault();
                const formData = new FormData(event.target);
                const chatName = formData.get('chat-name');
                const participants = formData.get('participants').split(',');
                const newChatElement = document.createElement('div');
                newChatElement.classList.add('chat-message');
                newChatElement.dataset.chatInfo = formData.get('participants');
                newChatElement.innerHTML = `
                    <input type="hidden" name="id" id="chat-id-">${participants}<" />
                    <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                    <span class="chat-username">${chatName}</span>
                `;
                id_num++;
                document.getElementById('chat-block').appendChild(newChatElement);
                toggleModal(addChatModalWrapper);
        
                // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
                openChatOnClick();
            });
        
            const cancelAddChatButton = document.getElementById("add-chat-btn-close");
            cancelAddChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
            addClickListenersToMessages();
        });
        
        document.getElementById('chat-block').appendChild(newChatElement);
        toggleModal(addChatModalWrapper);

        // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
        openChatOnClick();
    });

    const cancelAddChatButton = document.getElementById("add-chat-btn-close");
    cancelAddChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
    addClickListenersToMessages();
});
