document.addEventListener("DOMContentLoaded", function () {
    // Перевірка, чи є email в URL
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const email = urlParams.get('email');

    // Якщо email не існує, перенаправляємо на сторінку авторизації
    if (!email) {
        window.location.href = "login.html";
        return;
    }

    const socket = io('http://127.0.0.1:3000'); // URL сервера WebSocket

    socket.emit('login', email);

    // Логування помилок на клієнті
    ['connect_error', 'connect_timeout', 'error'].forEach(event => {
        socket.on(event, (error) => {
            console.error(`Socket ${event} error:`, error);
        });
    });

    const notificationButton = document.getElementById("notifications-button");

    notificationButton.ondblclick = () => {
        notificationButton.animate([
            { transform: "rotate(0)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(30deg)" },
            { transform: "rotate(0)" }
        ], {
            duration: 500,
            iterations: 1
        });
    };

    let isClicked= false;
    notificationButton.addEventListener('click', () => {
        const modalProfile = document.getElementById("modal-notifications");
        toggleModal(modalProfile);
        isClicked = !isClicked;
    });

    const userNameElement = document.getElementById('user-name');
    userNameElement.textContent = email;

    const userElement = document.getElementById('user');
    userElement.classList.remove("hidden");

    let isProfileOpened = false;
    const profileButton = document.getElementById("user");

    const toggleModal = (modalWindow) => {
        modalWindow.classList.toggle("hidden");
    };

    profileButton.addEventListener('click', () => {
        const modalProfile = document.getElementById("modal-profile");
        toggleModal(modalProfile);
        isProfileOpened = !isProfileOpened;
    });

    document.getElementById('log-out-modal-btn').addEventListener('click', function () {
        window.location.href = "login.html";
    });

    function addMessageToChat(message, isSent) {
        const selectedChat = document.querySelector('.chat-message.selected');
        if (!selectedChat) return; // Якщо немає вибраного чату, вийти з функції
    
        const chatInfo = selectedChat.dataset.chatInfo;
        const recipients = chatInfo.split(',').map(recipient => recipient.trim());
    
        // Перевірка, чи відкритий чат з одним єдиним одержувачем (не "all")
        if (recipients.length !== 1 || recipients.includes('all')) return;
    
        // Перевірка, чи відправлене повідомлення для відкритого чату або чи він відповідає поточному користувачу
        if((!isSent&&recipients.includes(message.sender))||isSent){
            const messagesContainer = document.getElementById('chat-room-admin-messages');
            const messageElement = document.createElement('div');
            messageElement.classList.add('chat-message', isSent ? 'sent' : 'received');
            messageElement.innerHTML = `
                <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                <span class="chat-username">${message.sender === email ? 'You' : message.sender}</span>
                <p class="message-text">${message.text}</p>
            `;
            messagesContainer.appendChild(messageElement);
            messagesContainer.scrollTop = messagesContainer.scrollHeight;
        }
    }    
    
   // Функція для створення унікального ідентифікатора чату на основі айді відправника та отримувача
    function generateChatId(senderId, recipientId) {
        const sortedIds = [senderId, recipientId].sort(); // Сортуємо айді в алфавітному порядку
        return sortedIds.join('-'); // Об'єднуємо айдішки для створення унікального чат-айді
    }

    // Функція для зберігання повідомлень у локальному сховищі за айді чату
    function addMessageToLocalStorage(senderId, recipientId, message) {
        const chatId = generateChatId(senderId, recipientId);
        let messages = localStorage.getItem(chatId);
        if (!messages) {
            messages = [];
        } else {
            messages = JSON.parse(messages);
        }
        messages.push(message);
        localStorage.setItem(chatId, JSON.stringify(messages));
    }

    // Функція для отримання повідомлень з локального сховища за айді чату
    function getMessagesFromLocalStorage(senderId, recipientId) {
        const chatId = generateChatId(senderId, recipientId);
        const messages = localStorage.getItem(chatId);
        return messages ? JSON.parse(messages) : [];
    }

    function sendMessage(messageText, recipients) {
        try {
            socket.emit('message', { sender: email, text: messageText, recipients: recipients });
            const newMessage = { sender: email, text: messageText };
            if (!recipients.includes('all') && !recipients.includes(email)) { // Перевірка, чи ви не є одним з отримувачів
                addMessageToChat(newMessage, true);
            }
            document.getElementById("message-input").value = ''; // Очищення поля вводу після надсилання
        } catch (error) {
            console.error('Error while sending message:', error);
            alert('Failed to send message. Please try again later.');
        }
    }    
    
    // Функція для додавання прослуховувача кліків до повідомлень чату
    function addClickListenersToMessages() {
        const chatMessages = document.querySelectorAll('.chat-message');
        chatMessages.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                const chatInfo = message.dataset.chatInfo;
                openChat(chatInfo);
            });
        });
    }

    socket.on('messageResponse', (response) => {
        if (response.success) {
            const newMessage = { sender: response.data.sender, text: response.data.text };
            console.log("Sender:", newMessage.sender);
            console.log("Text:", newMessage.text);
            addMessageToChat(newMessage, response.data.sender===email); // Додати повідомлення у чат як отримане
            addMessageToLocalStorage(response.data.sender, email, newMessage); // Зберегти повідомлення у локальне сховище для поточного отримувача
            document.getElementById("message-input").value = ''; // Очищення поля вводу після отримання відповіді
            addClickListenersToMessages();
            
            // Отримуємо елемент спливаючого вікна
            const notificationsWindow = document.getElementById("modal-notifications");
            
            // Перевіряємо, чи вікно приховане, і якщо так, то змінюємо його стиль, щоб воно стало видимим
            if (notificationsWindow.classList.contains("hidden")) {
                notificationsWindow.classList.remove("hidden");
            }
            
            // Створюємо новий елемент для відображення останнього повідомлення
            const messageElement = document.createElement('div');
            messageElement.classList.add('notification-item');
            messageElement.innerHTML = `
                <div class="notification-sender">${newMessage.sender}</div>
                <div class="notification-text">${newMessage.text}</div>
                <i class="fa fa-times notification-close"></i>
            `;
            
            // Додаємо нове повідомлення до початку спливаючого вікна
            notificationsWindow.insertBefore(messageElement, notificationsWindow.firstChild);
            
            // Додаємо обробник подій для кожного хрестика
            const closeButtons = document.querySelectorAll('.notification-close');
            closeButtons.forEach(button => {
                button.addEventListener('click', () => {
                    const notificationItem = button.parentNode; // Отримуємо батьківський елемент (повідомлення)
                    notificationItem.remove(); // Видаляємо повідомлення
                });
            });
        } else {
            console.log('Message failed to send:', response.error);
            alert(response.error);
        }
    });       

    const sendButton = document.getElementById("send-button");
    sendButton.addEventListener('click', () => {
        const messageInput = document.getElementById("message-input");
        const messageText = messageInput.value.trim();
        if (messageText !== '') {
            const chatInfo = document.querySelector('.chat-message.selected').dataset.chatInfo;
            const recipients = chatInfo.split(',').map(recipient => recipient.trim());
            sendMessage(messageText, recipients);
        }
        addClickListenersToMessages();
    });

    function openChat(chatInfo) {
        const participants = chatInfo.split(',').map(recipient => recipient.trim());
        updateMembers(participants);
        document.getElementById('chat-room-admin-header').innerHTML = `<h2>Chat Room with ${participants.join(', ')}</h2>`;
        document.getElementById('chat-room-admin').style.display = 'flex';
        if(participants.length===1){
            const messages = getMessagesFromLocalStorage(email, participants[0]);
            const messagesContainer = document.getElementById('chat-room-admin-messages');
            messagesContainer.innerHTML = '';
            messages.forEach(message => {
                const messageElement = document.createElement('div');
                messageElement.classList.add('chat-message', message.sender === email ? 'sent' : 'received');
                messageElement.innerHTML = `
                <div class="msg-info">
                    <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
                    <span class="username">${message.sender === email ? 'You' : message.sender}</span>
                </div>
                <p class="text">${message.text}</p>`;
                messagesContainer.appendChild(messageElement);
            });
        }
        addClickListenersToMessages();
    }    

    function updateMembers(participants) {
        const membersContainer = document.getElementById('chat-room-admin-members');
        membersContainer.innerHTML = '';
        participants.forEach(participant => {
            const userIcon = document.createElement('i');
            userIcon.classList.add('fa', 'fa-user-circle');
            membersContainer.appendChild(userIcon);
        });
        const plusIcon = document.createElement('i');
        plusIcon.classList.add('fa', 'fa-plus-circle');
        membersContainer.appendChild(plusIcon);
    }

    function openChatOnClick() {
        const newChatElement = document.querySelectorAll('.chat-message');
        newChatElement.forEach(message => {
            message.addEventListener('click', () => {
                document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
                message.classList.add('selected');
                const chatInfo = message.dataset.chatInfo;
                openChat(chatInfo);
            });
        });
    }

    const chatMessages = document.querySelectorAll('.chat-message');
    chatMessages.forEach(message => {
        message.addEventListener('click', () => {
            document.querySelectorAll('.chat-message').forEach(item => item.classList.remove('selected'));
            message.classList.add('selected');
            const chatInfo = message.dataset.chatInfo;
            openChat(chatInfo);
        });
    });

    const addChatButton = document.getElementById("new-chat-room-link");
    const addChatModalWrapper = document.getElementById("add-chat");
    const addChatForm = document.getElementById("add-chat-form");
    const addChatModal = addChatModalWrapper?.querySelector(".modal");

    addChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
    addChatForm.addEventListener('submit', event => {
        event.preventDefault();
        const formData = new FormData(event.target);
        const chatName = formData.get('chat-name');
        const participants = formData.get('participants').split(',');
        const newChatElement = document.createElement('div');
        newChatElement.classList.add('chat-message');
        newChatElement.dataset.chatInfo = formData.get('participants');
        newChatElement.innerHTML = `
            <img src="https://www.gravatar.com/avatar/00000000000000000000000000000000?s=100&d=mp" alt="User icon" class="chat-icon">
            <span class="chat-username">${chatName}</span>
        `;
        document.getElementById('chat-block').appendChild(newChatElement);
        toggleModal(addChatModalWrapper);

        // Після створення нового чату, додайте прослуховувач подій для відкриття чату при кліку на нього
        openChatOnClick();
    });

    const cancelAddChatButton = document.getElementById("add-chat-btn-close");
    cancelAddChatButton.addEventListener('click', () => toggleModal(addChatModalWrapper));
    addClickListenersToMessages();
});
