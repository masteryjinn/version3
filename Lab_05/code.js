

document.addEventListener("DOMContentLoaded", async function() {    
    const studentsTable = document.getElementById("students-table");
    let lastStudentId = 1; 

    await loadStudentsFromDatabase();
    
    const notificationButton = document.getElementById("notifications-button");
    const notificationsModal= document.getElementById("modal-notifications");
    const profileButton = document.getElementById("user");
    const addStudentButton = document.getElementById("add-student-btn");
    const addStudentModal = document.getElementById("add-student");
    const addStudentForm = document.getElementById("add-student-form");
    const cancelAddStudentButton = document.getElementById("add-student-btn-close");
    const labelAddEdit = document.getElementById("add-student-label");
    const labelDelete = document.getElementById("delete-student-label");

    let isProfileOpened = false;
    let currentStudentRow = null; // Global variable to store the current student row being edited
    

    notificationButton.ondblclick = () => {
        notificationButton.animate([
            { transform: "rotate(0)" },
            { transform: "rotate(-30deg)" },
            { transform: "rotate(30deg)" },
            { transform: "rotate(0)" }
        ], {
            duration: 500,
            iterations: 1
        });
    };

    //notificationButton.onmouseover = () => show(document.getElementById("modal-notifications"));
    //notificationButton.onmouseleave = () => hide(document.getElementById("modal-notifications"));
    notificationButton.addEventListener('click', function() {
        // Your existing code to toggle the notifications modal
        notificationsModal.classList.toggle('hidden');
    
        // Check if the notifications modal is hidden
        if (notificationsModal.classList.contains('hidden')) {
            // If hidden, do nothing
            return;
        }
    
        // If the notifications modal is visible, redirect to the messages page
        const messagesLink = document.getElementById('messages-link');
        messagesLink.click();
    });

    profileButton.onclick = () => {
        if (!isProfileOpened) {
            show(document.getElementById("modal-profile"));
        } else {
            hide(document.getElementById("modal-profile"));
        }
        isProfileOpened = !isProfileOpened;
    };

    addStudentButton.onclick = () => {
        if (currentStudentRow) {
            addStudentForm.reset();
            currentStudentRow = null; // Reset current student row
        }
        show(addStudentModal);
    };

    async function sendDataToServer(data, method = 'POST') {
        try {
            const url = 'http://localhost:3000/student';
            const response = await fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            });
    
            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(errorData.message);
            }
    
            const responseData = await response.json();
            console.log("Student data sent successfully");
            return responseData;
        } catch (error) {
            console.error("Error sending data:", error);
            throw error;
        }
    }
    
    
    
    addStudentForm.onsubmit = async (event) => {
        event.preventDefault();
        const formData = new FormData(event.target);
        try {
            let response; // Оголошуємо змінну для результату запиту
            if (currentStudentRow) {
                console.log(currentStudentRow.id);
                const editStudent = {
                    id: currentStudentRow.id,
                    group: formData.get('group'),
                    name: formData.get('name'),
                    surname: formData.get('surname'),
                    gender: formData.get('gender'),
                    birthday: formData.get('birthday')
                };
                // Відправляємо дані на сервер методом PUT
                response = await sendDataToServer(editStudent, 'PATCH');
            } else {
                const addStudent = {
                    id: lastStudentId,
                    group: formData.get('group'),
                    name: formData.get('name'),
                    surname: formData.get('surname'),
                    gender: formData.get('gender'),
                    birthday: formData.get('birthday')
                };
                response = await sendDataToServer(addStudent);
            }
    
            if (response && response.success) {
                // Додаємо нового студента у таблицю
                addOrUpdateStudent(Object.fromEntries(formData));
                hide(addStudentModal);
                labelAddEdit.textContent = "Add student";
                console.log("Student updated successfully");
            }
        } catch (error) {
            alert(error); // Виводимо текст помилки
        }
    };
    
    

    cancelAddStudentButton.onclick = () => {
        hide(addStudentModal);
        labelAddEdit.textContent="Add student";
    };

    async function removeStudent(studentId) {
        const studentRow = document.getElementById(studentId);
        if (studentRow) {
            const group = studentRow.cells[1].textContent;
            const nameSurname = studentRow.cells[2].textContent;
            const [name, surname] = nameSurname.split(" ");
            const gender = studentRow.cells[3].textContent;
            const birthday = studentRow.cells[4].textContent;
    
            // Create an object with the deleted student's information
            const deletedStudent = {
                id: studentId,
                group: group,
                name: name,
                surname: surname,
                gender: gender,
                birthday: birthday
            };
    
            // Відправка даних на сервер для видалення студента
            try {
                await sendDataToServer(deletedStudent, 'DELETE');
                console.log(`Student with ID ${studentId} deleted successfully`);
                studentRow.remove(); // Видалення рядка з таблиці
            } catch (error) {
                alert(error); // Виводимо текст помилки
            }
        }
    }    

    function addOrUpdateStudent(formData) {
        const studentId = formData.id;
        const group = formData.group;
        const name = formData.name;
        const surname = formData.surname;
        const gender = formData.gender;
        const birthday = formData.birthday;

        // Check if we are editing an existing student
        if (currentStudentRow) {
            currentStudentRow.cells[1].textContent = group;
            currentStudentRow.cells[2].textContent = `${name} ${surname}`;
            currentStudentRow.cells[3].textContent = gender;
            currentStudentRow.cells[4].textContent = birthday;
            // Reset currentStudentRow after editing
            currentStudentRow = null;
        } else {
            // Add new student to the table
            const newStudentId = lastStudentId;
            const studentData = { id: newStudentId, group, name, surname, gender, birthday };
            const studentDataString = addStudent(studentData);
            console.log(studentDataString);
            lastStudentId++;
        }
        addStudentForm.reset();
    }        

    /*function validateForm() {
        const name = document.getElementById("name").value.trim();
        const surname = document.getElementById("surname").value.trim();
        const doubleRegex = /^[A-Z][a-z]*(-[A-Z][a-z]*)?$/;
    
        if (!doubleRegex.test(name) || !doubleRegex.test(surname)) {
            alert("Please ensure the first letter of the name is capitalized and the surname follows the correct format.");
            return false; 
        }

        return true;
    }*/
    

    function addStudent({ id, group, name, surname, gender, birthday }) {
        let tr = document.getElementById(id);
    
        if (!tr) {
            // Create elements for the new student row if it doesn't exist
            tr = document.createElement("tr");
            tr.setAttribute('id', id); // Set the ID for the student row
    
            const checkboxTd = document.createElement("td");
            const groupTd = document.createElement("td");
            const nameSurnameTd = document.createElement("td");
            const genderTd = document.createElement("td");
            const birthdayTd = document.createElement("td");
            const statusTd = document.createElement("td");
            const actionsContainer = document.createElement("td");
    
            const checkbox = document.createElement("input");
            const editButton = document.createElement("button");
            const deleteButton = document.createElement("button");
    
            editButton.classList.add("student__btn");
            editButton.id = "edit-student-btn";
            deleteButton.classList.add("student__btn");
            deleteButton.id = "delete-student-btn";
    
            checkbox.type = "checkbox";
            checkboxTd.appendChild(checkbox);
            groupTd.textContent = group;
            nameSurnameTd.textContent = `${name} ${surname}`;
            genderTd.textContent = gender;
            birthdayTd.textContent = new Date(birthday).toLocaleDateString(); // Format birthday
            statusTd.innerHTML = '<i class="fa fa-circle" style="color: #d8d8d8;"></i>';
            editButton.innerHTML = '<i class="fa fa-pencil btn__icon" aria-hidden="true"></i>';
            deleteButton.innerHTML = '<i class="fa fa-trash btn__icon" aria-hidden="true"></i>';
    
            deleteButton.addEventListener("click", () => {
                const studentRow = document.getElementById(id);
                if (studentRow) {
                    const nameSurname = studentRow.cells[2].textContent;
                    const [name, surname] = nameSurname.split(" ");
                    labelDelete.textContent = "Delete user " + name + " " + surname + " ?"; 
                    show(document.getElementById("delete-warn-student"));
                    const cancelModalButton = document.getElementById("cancel-modal-btn");
                    const deleteModalButton = document.getElementById("delete-modal-btn");
            
                    cancelModalButton.onclick = () => hide(document.getElementById("delete-warn-student"));
            
                    deleteModalButton.onclick = () => {
                        removeStudent(id);
                        hide(document.getElementById("delete-warn-student"));
                    };
                }
            });
            
    
            editButton.addEventListener("click", () => {
                labelAddEdit.textContent="Edit student";
                populateEditForm(id);
                show(addStudentModal);
            });
    
            actionsContainer.classList.add("table_buttons");
            actionsContainer.appendChild(editButton);
            actionsContainer.appendChild(deleteButton);
    
            tr.appendChild(checkboxTd);
            tr.appendChild(groupTd);
            tr.appendChild(nameSurnameTd);
            tr.appendChild(genderTd);
            tr.appendChild(birthdayTd);
            tr.appendChild(statusTd);
            tr.appendChild(actionsContainer);
    
            studentsTable.appendChild(tr); // Append the student row to the table
        }
    
        // Return the created or existing student row
        return tr;
    }
    
    function populateEditForm(studentId) {
        const studentRow = document.getElementById(studentId);
        if (studentRow) {
            const group = studentRow.cells[1].textContent;
            const nameSurname = studentRow.cells[2].textContent;
            const [name, surname] = nameSurname.split(" ");
            const gender = studentRow.cells[3].textContent;
            const birthday = studentRow.cells[4].textContent;

            document.getElementById("group").value = group;
            document.getElementById("name").value = name;
            document.getElementById("surname").value = surname;
            document.getElementById("gender").value = gender;
            document.getElementById("birthday").value = birthday;

            currentStudentRow = studentRow; // Set the current student row
            //const studentId = studentRow.id;
            //console.log("Student ID: ", studentId);
        }
    }

    function show(modalWindow) {
        modalWindow.classList.remove("hidden");
    }

    function hide(modalWindow) {
        modalWindow.classList.add("hidden");
    }

    async function loadStudentsFromDatabase() {
        try {
            const url = 'http://localhost:3000/students'; // Припустимий URL для отримання даних студентів
            const response = await fetch(url ,{method:'GET'}); 
            if (!response.ok) {
                throw new Error('Failed to fetch students data'); 
            }
            const studentsData = await response.json();
            studentsData.forEach(student => {
                // Перетворення поля group_name на group
                student.group = student.group_name;
                student.id=lastStudentId;
                delete student.group_name; // Видалення старого поля
                const studentDataString = addStudent(student); // Додавання кожного студента до таблиці
                console.log(studentDataString);
                lastStudentId++;
            });
            console.log("Students data loaded successfully");
            // Після завершення завантаження даних та побудови таблиці:
            document.querySelector('.students').classList.remove('hidden');
        } catch (error) {
            console.error("Error loading students data:", error);
            // Обробка помилки
        }
    }
});

