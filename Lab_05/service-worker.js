const CACHE_NAME = 'my-cache';//13
   
const urlsToCache = [           
    '/',             
    '/index.html',           
    '/style1.css',                                 
    '/code.js',            
    '/chat.html',                                          
    '/script.js',                                 
    '/style2.css',
    '/login.html',                  
    '/login.js',                                     
    '/style3.css'                                                                
];                                        
                       
self.addEventListener('install', event => {       
    event.waitUntil(                                                                   
        caches.open(CACHE_NAME)                                
            .then(cache => cache.addAll(urlsToCache))               
    );                           
});                
            
self.addEventListener('fetch', event => {   
    // Ігноруємо POST-запити і запити методом PUT та PATCH
    if (event.request.method === 'POST' || event.request.method === 'PUT' || event.request.method === 'PATCH'||event.request.method === 'DELETE') {         
        return;                           
    }                                                                                                                                     
                            
    event.respondWith(                                                                                                                   
        caches.match(event.request)  
            .then(cachedResponse => {           
                // Повертаємо кешовану відповідь, якщо вона є           
                if (cachedResponse) {       
                    return cachedResponse;                         
                }                        
                // Якщо ресурсу немає в кеші, виконуємо запит до мережі
                return fetch(event.request)  
                    .then(response => { 
                        // Клонуємо відповідь, щоб використати її одночасно у кеші і для відображення
                        const responseToCache = response.clone();                                                                         
                        // Зберігаємо відповідь в кеші
                        caches.open(CACHE_NAME)     
                            .then(cache => {              
                                cache.put(event.request, responseToCache);
                            });  
       
                        // Повертаємо відповідь мережі                
                        return response;       
                    })      
                    .catch(error => {
                        console.error('Failed to fetch:', error);
                        // Повертаємо пусту відповідь, якщо запит не вдалося обробити
                        return new Response(null, { status: 404, statusText: 'Not Found' });
                    });
            })
    );                               
}); 
                                                                    