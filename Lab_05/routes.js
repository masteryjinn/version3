// routes.js

import fs from 'fs';
import mysql from 'mysql2/promise';

const connection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: '12345678',
    database: 'students_database'
});

// Read the content of scheme.sql as a string
const sqlQueries = fs.readFileSync('scheme.sql', 'utf8');

// Split the content into individual queries
const queries = sqlQueries.split(';').filter(query => query.trim() !== ''); // Filter out empty queries

// Function to execute each query in sequence
async function executeQueries(queries) {
    for (const query of queries) {
        try {
            await connection.query(query);
        } catch (error) {
            console.error('Failed to execute query:', error);
            throw error; // Propagate the error
        }
    }
}

async function getAllStudents() {
    const [rows] = await connection.query("select * from Students")
    return rows
}
// Function to define routes
export async function defineRoutes(app) {
    try {
        await executeQueries(queries);
        console.log('Database and tables created successfully');
        // Define routes
        app.get("/students", async (req, res) => {
            console.log('get');
            const students = await getAllStudents()
            console.log(students);
            res.send(students)
            })

        app.post('/student', async (req, res) => {
            const studentData = req.body;
            console.log(studentData);
            
            // Ensure each required property is present in the request body
            if (!studentData.hasOwnProperty('id') || 
                !studentData.hasOwnProperty('group') || 
                !studentData.hasOwnProperty('name') || 
                !studentData.hasOwnProperty('surname') || 
                !studentData.hasOwnProperty('gender') || 
                !studentData.hasOwnProperty('birthday')) {
                    return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
            }

            // Перевірка коректності даних студента
            if (!isValidStudentData(studentData)) {
                return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
            }

            try {
                const { id, group, name, surname, gender, birthday } = studentData;
                const result = await connection.query('INSERT INTO Students (id, group_name, name, surname, gender, birthday) VALUES (?, ?, ?, ?, ?, ?)', [id, group, name, surname, gender, birthday]);
                res.json({ success: true, message: 'Student added successfully' });
            } catch (error) {
                console.error('Error adding student:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to add student to the database' });
            }
        });

        app.patch('/student', async (req, res) => {
            const studentData = req.body; 
            console.log(studentData);
            
            // Ensure each required property is present in the request body
            if (!studentData.hasOwnProperty('id') || 
                !studentData.hasOwnProperty('group') || 
                !studentData.hasOwnProperty('name') || 
                !studentData.hasOwnProperty('surname') || 
                !studentData.hasOwnProperty('gender') || 
                !studentData.hasOwnProperty('birthday')) {
                    return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
            }

                // Перевірка коректності даних студента
            if (!isValidStudentData(studentData)) {
                return res.status(400).json({ error: 'Invalid student data', message: 'Please ensure the first letters of the name and the surname are capitalized and follow the correct format.' });
            }
        
            try {
                const { id, group, name, surname, gender, birthday } = studentData;
                const result = await connection.query('UPDATE Students SET group_name = ?, name = ?, surname = ?, gender = ?, birthday = ? WHERE id = ?', [group, name, surname, gender, birthday, id]);
                res.json({ success: true, message: 'Student data updated successfully' });
            } catch (error) {
                console.error('Error updating student data:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to update student data' });
            }
        });
        

        app.delete('/student', async (req, res) => {
            const studentId = parseInt(req.body.id, 10); 

            try {
                const result = await connection.query('DELETE FROM Students WHERE id = ?', studentId);
                res.json({ success: true, message: `Student with ID ${studentId} deleted successfully` });
            } catch (error) {
                console.error('Error deleting student:', error);
                res.status(500).json({ error: 'Database error', message: 'Failed to delete student from the database' });
            }
        });
    } catch (error) {
        console.error('Failed to initialize routes:', error);
        throw new Error('Failed to initialize routes:', error);
    }
}

// Функція для перевірки коректності даних студента
//поле айді пусте, бо назначається у функції додавання
function isValidStudentData(studentData) {
    const name = studentData.name.trim();   
    const surname = studentData.surname.trim();
    const doubleRegex = /^[A-Z][a-z]*(-[A-Z][a-z]*)?$/;
    
    if (!doubleRegex.test(name) || !doubleRegex.test(surname)) {
        //alert("Please ensure the first letter of the name is capitalized and the surname follows the correct format.");
        return false; 
    }

    return true;
}

export { connection }; // Export the connection variable

