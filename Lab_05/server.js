import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import { defineRoutes, connection } from './routes.js';
import { Server as SocketIOServer } from 'socket.io';

const app = express();
const port = 3000;
const server = http.createServer(app);

const users = {};

app.use(bodyParser.json());

// CORS middleware
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5501');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

async function startServer() {
    try {
        await defineRoutes(app);
        console.log('Database and tables created successfully');
        await checkDatabaseExists();
        
        server.listen(port, () => {
            console.log(`Server is running on port ${port}`);
        });

        const io = new SocketIOServer(server, {
            cors: {
                origin: "http://127.0.0.1:5501",
                methods: ["GET", "POST"],
                credentials: true
            }
        });

        // При підключенні нового користувача
        io.on('connection', (socket) => {
            socket.on('login', (email) => {
                users[email] = socket.id;
                console.log(`User ${email} connected`);
            });

            socket.on('message', (data) => {
                console.log('Message received:', data);
                const { sender, text, recipients } = data;
                if (recipients.includes('all')) {
                    io.emit('messageResponse', { success: true, message: 'Message sent to all students', data: data });
                }else{
                recipients.forEach(recipient => {
                    const recipientSocketId = users[recipient];
                    if (recipientSocketId) {
                        io.to(recipientSocketId).emit('messageResponse', { success: true, data: { sender, text } });
                    } else {
                        io.to(socket.id).emit('messageResponse', { success: false, error: 'Recipient not found' });
                    }
                });
            }
            });

            socket.on('disconnect', () => {
                const user = Object.keys(users).find(key => users[key] === socket.id);
                if (user) {
                    delete users[user];
                    console.log(`User ${user} disconnected`);
                }
            });
        });
    } catch (error) {
        console.error('Failed to start server:', error);
        process.exit(1);
    }
}

startServer();

async function checkDatabaseExists() {
    try {
        const [rows, fields] = await connection.query("SHOW DATABASES LIKE 'students_database'");
        if (rows.length === 0) {
            await executeQueries(queries);
            console.log('Database and tables created successfully');
        } else {
            console.log('Database and tables already exist');
        }
    } catch (error) {
        console.error('Failed to check database existence:', error);
        throw error;
    }
}
