

/*// studentController.js

import { connection } from './routes.js'; // Import database connection

let id=1;
// Controller function to add a new student
export async function addStudent(req, res) {
    const studentData = req.body;

    // Ensure the required properties are present in the request body
    const { group, name, surname, gender, birthday } = studentData;
    if (!group || !name || !surname || !gender || !birthday) {
        return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
    }

    try {
        const result = await connection.query('INSERT INTO Students (id, group_name, name, surname, gender, birthday) VALUES (?, ?, ?, ?, ?, ?)', [id, group, name, surname, gender, birthday]);
        res.json({ success: true, message: 'Student added successfully' });
        id++;
    } catch (error) {
        console.error('Error adding student:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to add student to the database' });
    }
}

// Controller function to update student data
export async function updateStudent(req, res) {
    const studentId = req.params.id;
    const studentData = req.body;

    // Ensure the required properties are present in the request body
    const { group, name, surname, gender, birthday } = studentData;
    if (!group || !name || !surname || !gender || !birthday) {
        return res.status(400).json({ error: 'Invalid student data', message: 'All fields are required' });
    }

    try {
        const result = await connection.query('UPDATE Students SET group_name = ?, name = ?, surname = ?, gender = ?, birthday = ? WHERE id = ?', [group, name, surname, gender, birthday, studentId]);
        res.json({ success: true, message: 'Student data updated successfully' });
    } catch (error) {
        console.error('Error updating student data:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to update student data' });
    }
}

// Controller function to delete a student
export async function deleteStudent(req, res) {
    const studentId = req.params.id;

    try {
        const result = await connection.query('DELETE FROM Students WHERE id = ?', studentId);
        res.json({ success: true, message: `Student with ID ${studentId} deleted successfully` });
    } catch (error) {
        console.error('Error deleting student:', error);
        res.status(500).json({ error: 'Database error', message: 'Failed to delete student from the database' });
    }
}*/
