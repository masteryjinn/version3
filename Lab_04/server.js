// server.js
import express from 'express';
import bodyParser from 'body-parser';
import { defineRoutes, connection } from './routes.js'; // Import defineRoutes and connection

const app = express();
const port = 3000;

// Middleware to parse JSON data
app.use(bodyParser.json());

// CORS middleware
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', 'http://127.0.0.1:5501');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

async function checkDatabaseExists() {
    try {
        // Check for the existence of the database
        const [rows, fields] = await connection.query("SHOW DATABASES LIKE 'students_database'");
        if (rows.length === 0) {
            // Database does not exist, create the database and tables
            await executeQueries(queries);
            console.log('Database and tables created successfully');
        } else {
            console.log('Database and tables already exist');
        }
    } catch (error) {
        console.error('Failed to check database existence:', error);
        throw error;
    }
}

// Function to start the server
async function startServer() {
    try {
        await defineRoutes(app); // Pass app as a parameter
        console.log('Database and tables created successfully');
        await checkDatabaseExists(); // Перевірка існування бази даних
        // Start the server
        app.listen(port, () => {
            console.log(`Server is running on port ${port}`);
        });
    } catch (error) {
        console.error('Failed to start server:', error);
        process.exit(1); // Exit the process with a non-zero status code
    }
}

// Start the server
startServer();
/*INSERT INTO Students (group_name, name, surname, gender, birthday) 
VALUES 
('Group A', 'John', 'Doe', 'Male', '1995-05-15'),
('Group B', 'Jane', 'Smith', 'Female', '1998-08-20'),
('Group A', 'Michael', 'Johnson', 'Male', '1997-03-10'),
('Group C', 'Emily', 'Brown', 'Female', '1996-11-25'); */