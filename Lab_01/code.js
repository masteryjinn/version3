const studentsTable = document.getElementById("students-table");
const notificationButton = document.getElementById("notifications-button");


notificationButton.ondblclick = () => {
    notificationButton.animate(
    [
        { transform: "rotate(0)" },
        { transform: "rotate(-30deg)" },
        { transform: "rotate(30deg)"},
        { transform: "rotate(0)"}
    ],
        {
          duration: 500,
          iterations: 1,
        }
      );
}


notificationButton.onmouseover = () => {
    show(document.getElementById("modal-notifications"));
}


notificationButton.onmouseleave = () => {
    hide(document.getElementById("modal-notifications"));
}


function show(modalWindow) {
    modalWindow.classList.remove("hidden");
}


function hide(modalWindow) {
    modalWindow.classList.add("hidden");
}


const profileButton = document.getElementById("user");


let isProfileOpened = false;
profileButton.onclick = () => {
    if(!isProfileOpened) {
        show(document.getElementById("modal-profile"));
        isProfileOpened = true;
    } else {
        hide(document.getElementById("modal-profile"));
        isProfileOpened = false;
    }
}


const addStudentButton = document.getElementById("add-student-btn");
const addStudentModalWrapper = document.getElementById("add-student");


const addStudentModal = addStudentModalWrapper?.querySelector(".modal");
const addStudentForm = document.getElementById("add-student-form");


function addStudent({group, name, surname, gender, birthday}) {
    const student = document.createElement("tr");


    const tr = document.createElement("tr");
    const checkboxTd = document.createElement("td");
    const groupTd = document.createElement("td");
    const nameSurnameTd = document.createElement("td");
    const genderTd = document.createElement("td");
    const birthdayTd = document.createElement("td");
    const statusTd = document.createElement("td");
    const actionsContainer = document.createElement("td");


    const checkbox = document.createElement("input");
    const editButton = document.createElement("button");
    const deleteButton = document.createElement("button");


    editButton.classList.add("student__btn");
    editButton.id = "edit-student-btn";
    deleteButton.classList.add("student__btn");
    deleteButton.id = "delete-student-btn";


    checkbox.type = "checkbox";
    checkboxTd.appendChild(checkbox);
    groupTd.textContent = group;
    nameSurnameTd.textContent = `${name} ${surname}`;
    genderTd.textContent = gender;
    birthdayTd.textContent = birthday;
    statusTd.innerHTML = '<i class="fa fa-circle" style="color: #d8d8d8;"></i>';
    editButton.innerHTML = '<i class="fa fa-pencil btn__icon" aria-hidden="true"></i>';
    deleteButton.innerHTML = '<i class="fa fa-trash btn__icon" aria-hidden="true"></i>';


    deleteButton.addEventListener("click", (element) => {
        show(document.getElementById("delete-warn-student"));
        const cancelModalButton = document.getElementById("cancel-modal-btn");
        const deleteModalButton = document.getElementById("delete-modal-btn");


        cancelModalButton.onclick = () => {
            hide(document.getElementById("delete-warn-student"));
        }


        deleteModalButton.onclick = () => {
            currentStudent.remove();
            hide(document.getElementById("delete-warn-student"));
        };
        currentStudent = element.target.closest("tr");
    });


    actionsContainer.classList.add("table_buttons");
    actionsContainer.appendChild(editButton);
    actionsContainer.appendChild(deleteButton);


    tr.appendChild(checkboxTd);
    tr.appendChild(groupTd);
    tr.appendChild(nameSurnameTd);
    tr.appendChild(genderTd);
    tr.appendChild(birthdayTd);
    tr.appendChild(statusTd);
    tr.appendChild(actionsContainer);


    studentsTable.appendChild(tr);

    return student;
}


addStudentButton.onclick = () => {
    show(document.getElementById("add-student"));
}


addStudentForm.onsubmit = (element) => {
    element.preventDefault();
    const formData = new FormData(element.target);
    const student = addStudent(Object.fromEntries(formData));
    hide(document.getElementById("add-student"));
};


const cancelAddStudentButton = document.getElementById("add-student-btn-close");


cancelAddStudentButton.onclick = () => {
    hide(document.getElementById("add-student"));
};


const editStudentButton = document.getElementById("edit-student-btn")


const deleteStudentButton = document.getElementById("delete-student-btn");
const deleteStudentModalWrapper = document.getElementsByClassName("delete-warn-student");